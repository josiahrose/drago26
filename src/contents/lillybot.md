---
title: Lilly Discord Bot
author: josiah
date: 2022-01-20
---

A custom Discord bot made with deno and typescript powered by MongoDB! It's a fun project and hopefully it goes far places because I am going to spend a lot of time into it.