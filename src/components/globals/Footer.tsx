import {
  Box,
  Container,
  SimpleGrid,
  Stack,
  Text,
  Tag,
  useColorModeValue,
} from "@chakra-ui/react";

import Link from "next/link";

import { ReactNode } from "react";

const ListHeader = ({ children }: { children: ReactNode }) => {
  return (
    <Text fontWeight={"500"} fontSize={"lg"} mb={2}>
      {children}
    </Text>
  );
};

export const Footer = () => {
  return (
    <Box
      px={{ sm: 1, md: 8 }}
      align={"center"}
      borderTop={"1px"}
      borderColor={useColorModeValue("gray.300", "gray.800")}
      w="100%"
      color={useColorModeValue("gray.700", "gray.200")}
    >
      <Container as={Stack} maxW={"6xl"} py={10}>
        <SimpleGrid columns={{ base: 2, sm: 2, md: 3 }} spacing={40}>
          <Stack>
            <ListHeader>Information</ListHeader>
            <Link href={"/about"}>About us</Link>
            <Link href={"/projects"}>Projects</Link>
            <Link href={"#"}>Client Portal</Link>
            <Link href={"/github"}>Github</Link>
          </Stack>
          <Stack>
            <ListHeader>Important</ListHeader>
            <Link href={"/contact"}>Contact Us</Link>
            <Link href={"#"}>Terms</Link>
            <Link href={"#"}>Privacy Policy</Link>
            <Link href={"#"}>Blog</Link>
          </Stack>
          <Stack>
            <ListHeader>Socials</ListHeader>
            <Link href={"#"}>Twitter</Link>
            <Link href={"#"}>Telegram</Link>
            <Link href={"/discord"}>Discord</Link>
            <Link href={"#"}>Coming Soon</Link>
          </Stack>
        </SimpleGrid>
      </Container>
      <Box py={10}>
        <Text pt={6} fontSize={{ base: "sm", md: "lg" }} textAlign={"center"}>
          &copy; {new Date().getFullYear()} Josiah Rose. All rights reserved
        </Text>
      </Box>
    </Box>
  );
};
