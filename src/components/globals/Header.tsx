import { useState, ReactNode } from "react";

import { CustomButton, DarkModeSwitch } from "components/utils";

import { CloseIcon, HamburgerIcon } from "@chakra-ui/icons";

import { Box, useColorModeValue, Flex, Text, Image } from "@chakra-ui/react";

import Link from "next/link";

type MenuProps = {
  children: ReactNode;
  href: string;
  color: string;
};

// @ts-ignore
const MenuItem = ({ children, href }: MenuProps) => {
  return (
    <Text
      mb={{ base: 0 ? 0 : 8, sm: 0 }}
      mr={{ base: 0, sm: 0 ? 0 : 8 }}
      display="block"
      _hover={{
        color: "gray.600",
      }}
    >
      <Link href={href}>{children}</Link>
    </Text>
  );
};

export const Header = () => {
  const [show, setShow] = useState(false);
  const toggleMenu = () => setShow(!show);

  const bg = useColorModeValue("gray.100", "gray.800");
  const color = useColorModeValue("black", "white");

  return (
    <Flex
      as={"nav"}
      align={"center"}
      justify={"space-between"}
      wrap={"wrap"}
      w="100%"
      color={{ base: color, lg: color }}
      bg={bg}
      p={"6"}
      mt={5}
      rounded={"md"}
      position={"relative"}
      zIndex={5}
      shadow={"md"}
    >
      <Box w="200px" display="flex" alignItems="center">
        <Link href="/">
          <Image src={"/me.png"} rounded={"md"} height={12} width={12} />
        </Link>
      </Box>

      <Box display={{ base: "block", md: "none" }} onClick={toggleMenu}>
        {show ? <CloseIcon /> : <HamburgerIcon />}
      </Box>

      <Box
        display={{ base: show ? "block" : "none", md: "block" }}
        flexBasis={{ base: "100%", md: "auto" }}
      >
        <Flex
          align="center"
          justify={["center", "space-between", "flex-end", "flex-end"]}
          direction={["column", "row", "row", "row"]}
          pt={[4, 4, 0, 0]}
        >
          <MenuItem href="/" color="black">
            Home
          </MenuItem>
          <MenuItem href="/about" color="black">
            About
          </MenuItem>
          <MenuItem href="#" color="black">
            Resume
          </MenuItem>
          <MenuItem href="/projects" color="black">
            Projects
          </MenuItem>
          <MenuItem href="/contact" color="black">
            Contact
          </MenuItem>
          <DarkModeSwitch />
        </Flex>
      </Box>
    </Flex>
  );
};
