import React, { ReactNode } from "react";
import { Container, Flex, useColorMode } from "@chakra-ui/react";

import { Header } from "@components/globals/Header";
import { Footer } from "@components/globals/Footer";

type ContainerProps = {
  children: ReactNode;
};

export const CustomContainer = ({ children }: ContainerProps) => {
  const { colorMode } = useColorMode();

  const bgColor = { light: "gray.50", dark: "gray.900" };

  const color = { light: "black", dark: "white" };
  return (
    <Flex
      as={Container}
      maxW={"full"}
      h={"100%"}
      direction="column"
      alignItems="center"
      justifyContent="center"
      mx="auto"
      bg={bgColor[colorMode]}
      color={color[colorMode]}
    >
      <Header />
      {children}
      <Footer />
    </Flex>
  );
};
