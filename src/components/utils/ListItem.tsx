import { ReactElement } from "react";
import { Text, Box, Heading, Stack, useColorModeValue } from "@chakra-ui/react";

import BlogPost from "../../types/BlogPost";

import Link from "next/link";

type Props = BlogPost;

export const ListItem = ({
  title,
  slug,
  date,
  excerpt,
}: Props): ReactElement => {
  return (
    <Link href={`/blog/${slug}`}>
      <Box
        bg={useColorModeValue("gray.200", "gray.800")}
        py={16}
        m={2}
        px={8}
        rounded={"xl"}
        align={"center"}
        justify={"center"}
        direction={"column"}
      >
        <Stack>
          <Heading fontSize={{ base: "md", md: "2xl" }}>{title}</Heading>
          <Text>{date}</Text>
          {excerpt && <Text className="_bloglist-excerpt">{excerpt} </Text>}
        </Stack>
      </Box>
    </Link>
  );
};
