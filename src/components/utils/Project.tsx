import {
  Text,
  Stack,
  Image,
  Center,
  Box,
  useColorModeValue,
} from "@chakra-ui/react";

import { CustomButton } from "./";

interface ProjectProps {
  title: string;
  text: string;
  redirect: string;
  image: string;
}

export const Project = ({ title, text, redirect, image }: ProjectProps) => {
  return (
    <Center py={12}>
      <Box
        role={"group"}
        p={6}
        maxW={"900px"}
        maxH={"600px"}
        h={"full"}
        w={"full"}
        bg={useColorModeValue("gray.100", "gray.800")}
        boxShadow={"2xl"}
        rounded={"lg"}
        pos={"relative"}
        zIndex={1}
      >
        <Box
          rounded={"lg"}
          mt={-12}
          right={6}
          bottom={3}
          pos={"relative"}
          height={"230px"}
          _after={{
            transition: "all .4s ease",
            content: '""',
            w: "full",
            h: "full",
            pos: "absolute",
            top: 5,
            left: 0,
            backgroundImage: `url(${image})`,
            filter: "blur(15px)",
            zIndex: -2,
          }}
          _groupHover={{
            _after: {
              filter: "blur(30px)",
            },
          }}
        >
          <Image
            rounded={"lg"}
            height={230}
            width={230}
            objectFit={"cover"}
            src={image}
          />
        </Box>
        <Stack pt={10} align={"center"}>
          <Text color={"gray.500"} fontSize={"lg"} textTransform={"uppercase"}>
            {title}
          </Text>
          <Text
            align={"center"}
            fontSize={"sm"}
            fontFamily={"body"}
            fontWeight={500}
          >
            {text}
          </Text>
          <a href={redirect}>
            <CustomButton>View</CustomButton>
          </a>
        </Stack>
      </Box>
    </Center>
  );
};

// bg={useColorModeValue("gray.100", "gray.800")}
