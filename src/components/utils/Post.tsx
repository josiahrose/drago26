import { ReactElement } from "react";
import { ListItem } from "./";

import BlogPost from "../../types/BlogPost";

type Props = {
  posts: BlogPost[];
};

export const Post = ({ posts }: Props): ReactElement => {
  return (
    <article>
      {posts.map((post, index) => (
        <ListItem key={index} {...post} />
      ))}
    </article>
  );
};
