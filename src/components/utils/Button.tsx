import React from "react";
import { Button, useColorModeValue } from '@chakra-ui/react'

export type ButtonProps = {
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void; // to handle onClick functions
  children?: React.ReactNode; // make the component able to receive children elements
  color?: "purple" | "blue" | "red" | "green"; // two styling options
};

export const CustomButton = ({
  onClick,
  children,
  color,
}: ButtonProps) => {
  return (
    <Button onClick={onClick} color={useColorModeValue("gray.900", "gray.100")} bg={color} colorScheme={color}>
      {children}
    </Button>
  );
};
