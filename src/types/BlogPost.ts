type BlogPost = {
  title: string;
  date: string;
  slug: string;
  excerpt?: string;
};

export default BlogPost;
