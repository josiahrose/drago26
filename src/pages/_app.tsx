import { ChakraProvider } from "@chakra-ui/react";
import type { AppProps } from "next/app";

import { DefaultSeo } from "next-seo";

import SEO from "../../next-seo.config";

import "@fontsource/josefin-sans";

import theme from "styles/theme";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <ChakraProvider resetCSS theme={theme}>
        <DefaultSeo {...SEO} />
        <Component {...pageProps} />
      </ChakraProvider>
    </>
  );
}

export default MyApp;
