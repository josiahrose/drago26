import Link from "next/link";
import BlogPost from "../types/BlogPost";

import fs from "fs";
import matter from "gray-matter";

import { GetStaticProps, NextPage } from "next";
import { Container, CustomButton, Feature, Post } from "@components/utils";
import { NextSeo } from "next-seo";
import { FcAssistant, FcDonate, FcInTransit } from "react-icons/fc";

import {
  Box,
  Stack,
  Heading,
  Text,
  Grid,
  SimpleGrid,
  Icon,
  Image,
  useColorModeValue,
} from "@chakra-ui/react";

type BlogProps = {
  posts: BlogPost[];
};

const Home: NextPage<BlogProps> = ({ posts }) => {
  return (
    <>
      <NextSeo title={"Home"} />

      <Container>
        <Stack
          as={Box}
          maxW={"3xl"}
          textAlign={"center"}
          align={"center"}
          spacing={{ base: 8, md: 14 }}
          py={{ base: 20, md: 36 }}
        >
          {/* This is the hello starter*/}
          <Image
            src={"/me.png"}
            w={64}
            h={64}
            rounded={"md"}
            alt={"Josiah Rose"}
            transitionProperty="shadow"
            transitionDuration="1"
            transitionTimingFunction="ease-in-out"
            _hover={{ shadow: "2xl" }}
          />
          <Heading fontWeight={600} fontSize={{ base: "3xl", md: "6xl" }}>
            Josiah Rose <br />
            <Text as={"span"} color={"green.400"}>
              Full-stack Developer
            </Text>
          </Heading>
          <Text color={"gray.500"}>
            Learn your project's potential by handing over the work to me. With
            every $ we earn, $4 gets donated to team seas in order to make the
            world a better place!
          </Text>
          <Stack direction={"row"} spacing={2} justify={"center"}>
            <Link href="/contact">
              <CustomButton color={"green"}>Hire me</CustomButton>
            </Link>
            <Link href="/about">
              <CustomButton color={"purple"}>Learn more</CustomButton>
            </Link>
          </Stack>
          {/* This is the hello starter end*/}
          <Stack as={Box} pb={20} pt={20}>
            <SimpleGrid columns={{ base: 2, md: 3 }} spacing={6}>
              <Feature
                icon={<Icon as={FcAssistant} w={10} h={10} />}
                title={"Lifetime Support"}
                text={
                  "Our team is dedicated to ensuring a quality service and support is provided to our clients..."
                }
              />
              <Feature
                icon={<Icon as={FcDonate} w={10} h={10} />}
                title={"Unlimited Donations"}
                text={
                  "With every $1 we earn, a bit gets donated to team seas in order to make the world a better place for animals..."
                }
              />
              <Feature
                icon={<Icon as={FcInTransit} w={10} h={10} />}
                title={"Fast Delivery"}
                text={
                  "We have fast high quality delivery to our clients as fast as we possibly can go while being professional..."
                }
              />
            </SimpleGrid>
          </Stack>
          {/* This is where a testimonial card starts*/}
          <Stack as={Box} w={"full"} pb={20} pt={20}>
            <Heading fontWeight={600} fontSize={{ base: "3xl", md: "5xl" }}>
              Blog Posts
            </Heading>
            <Grid spacing={3}>
              <Post posts={posts} />
            </Grid>
          </Stack>
        </Stack>
      </Container>
    </>
  );
};

export const getStaticProps: GetStaticProps<BlogProps> = async () => {
  const files = fs.readdirSync("src/contents");
  const posts = files.map((fname) => {
    const md = fs.readFileSync(`src/contents/${fname}`, "utf-8");

    const { data, excerpt } = matter(md, {
      excerpt_separator: "\r\n\r\n",
    });

    const date = data.date.toLocaleDateString("en", {
      year: "numeric",
      month: "long",
      day: "numeric",
    });

    return {
      title: data.title,
      slug: fname.replace(".md", ""),
      date,
      excerpt,
    };
  });
  return {
    props: {
      posts,
    },
  };
};

export default Home;
