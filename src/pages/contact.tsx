import type { NextPage } from "next";

import { Container, CustomButton } from "@components/utils";

import { NextSeo } from "next-seo";

import Link from "next/link";

import {
  Box,
  Stack,
  Text,
  Flex,
  Button,
  Textarea,
  FormControl,
  FormLabel,
  Input,
  useColorModeValue,
} from "@chakra-ui/react";

const Contact: NextPage = () => {
  return (
    <>
      <NextSeo title={"Contact"} />

      <Container>
        <Flex align={"center"} justify={"center"} minH={"60vh"}>
          <Stack
            mx={"auto"}
            spacing={8}
            maxW={"lg"}
            mt={12}
            mb={12}
            rounded={"xl"}
            py={12}
            px={6}
          >
            <Box
              rounded={"lg"}
              bg={useColorModeValue("gray.100", "gray.700")}
              boxShadow={"lg"}
              p={8}
            >
              <Stack spacing={4}>
                <FormControl id="email">
                  <FormLabel>Email address</FormLabel>
                  <Input type="email" />
                </FormControl>
                <FormControl id="email">
                  <FormLabel>Name</FormLabel>
                  <Input type="email" />
                </FormControl>
                <FormControl id="email">
                  <FormLabel>Inquiry</FormLabel>
                  <Textarea
                    borderColor="gray.300"
                    _hover={{
                      borderRadius: "gray.300",
                    }}
                    placeholder="Your content goes here please"
                  />
                </FormControl>
              </Stack>
              <Stack pt={8}>
                <CustomButton color={"blue"}>Disabled Button</CustomButton>
                <Link href={"/discord"}>
                  <CustomButton color={"green"}>Discord Server</CustomButton>
                </Link>
              </Stack>
            </Box>
          </Stack>
        </Flex>
      </Container>
    </>
  );
};

export default Contact;
