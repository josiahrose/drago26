import fs from "fs";
import path from "path";
import matter from "gray-matter";

import ReactMarkdown from "react-markdown";

import { GetStaticProps, GetStaticPaths, NextPage } from "next";
import { Container, CustomButton, Feature, Post } from "@components/utils";
import { NextSeo } from "next-seo";

interface BlogPostProps {
  content: string;
  frontmatter: {
    title: string;
    author: string;
    date: string;
  };
}

import {
  Box,
  Stack,
  Heading,
  Text,
} from "@chakra-ui/react";

const BlogPost: NextPage<BlogPostProps> = ({ frontmatter, content }) => {
  const { title, author, date } = frontmatter;
  return (
    <>
      <NextSeo title={title} />

      <Container>
        <Stack
          as={Box}
          maxW={"3xl"}
          textAlign={"center"}
          align={"center"} 
          mb={40}
          py={{ base: 20, md: 36 }}
        >
          <Heading>{title}</Heading>
          <Text>
            By {author} - {date}
          </Text>
          <Stack mt={4} w={"full"}>
            <ReactMarkdown>{content}</ReactMarkdown>
          </Stack>
        </Stack>
      </Container>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const files = fs.readdirSync("src/contents");
  const paths = files.map((fname) => ({
    params: {
      slug: fname.replace(".md", ""),
    },
  }));

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<BlogPostProps> = async ({
  params,
}) => {
  const slug = params?.slug;
  const md = fs
    .readFileSync(path.join("src/contents", `${slug}.md`))
    .toString();
  const { data, content } = matter(md);
  const date = data.date.toLocaleDateString("en", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });
  return {
    props: {
      frontmatter: {
        title: data.title,
        author: data.author,
        date,
      },
      content,
    },
  };
};

export default BlogPost;
