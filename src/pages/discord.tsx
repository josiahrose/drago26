import React, { useEffect } from "react";
import { useRouter } from "next/router";

const Discord = () => {
  const router = useRouter();

  useEffect(() => {
    router.push("https://discord.gg/YZKzRQ8EQz");
  }, []);
  return (
    <>
      <a>Redirecting to Discord Server</a>
    </>
  );
};

export default Discord;
