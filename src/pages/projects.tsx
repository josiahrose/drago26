import type { NextPage } from "next";

import { Container, CustomButton, Project } from "@components/utils";

import { NextSeo } from "next-seo";

import {
  Box,
  Stack,
  Heading,
  Text,
  Avatar,
  SimpleGrid,
  Icon,
  useColorModeValue,
} from "@chakra-ui/react";

const Projects: NextPage = () => {
  return (
    <>
      <NextSeo title={"Projects"} />

      <Container>
        <Stack
          as={Box}
          maxW={"3xl"}
          align={"center"}
          justify={"center"}
          spacing={{ base: 8, md: 14 }}
          py={{ base: 20, md: 36 }}
        >
          {/* This is the hello starter*/}
          <Heading fontWeight={600} fontSize={{ base: "4xl", md: "6xl" }}>
            Hire a professional <br />
            <Text as={"span"} color={"green.400"}>
              Look at our projects
            </Text>
          </Heading>
          {/* This is the hello starter end*/}
          <Stack as={Box} pb={20} pt={20}>
            <SimpleGrid columns={{ base: 2, md: 3 }} spacing={6}>
              <Project
                title={"PvPCore Bot"}
                text={
                  "A discord bot with interactive to manage a server."
                }
                redirect={"#"}
                image={"/bot.png"}
              />
              <Project
                title={"Lucky Bot"}
                text={
                  "A discord bot with slash to check domain information."
                }
                redirect={"#"}
                image={"/bot.png"}
              />
              <Project
                title={"Crush.moe"}
                text={
                  "An anime list that was never relased fully."
                }
                redirect={"#"}
                image={"/bot.png"}
              />
            </SimpleGrid>
          </Stack>
          {/* This is grid system end*/}
        </Stack>
      </Container>
    </>
  );
};

export default Projects;
