import type { NextPage } from "next";

import { Container } from "@components/utils";

import { NextSeo } from "next-seo";

import { Box, Stack, Text } from "@chakra-ui/react";

const About: NextPage = () => {
  return (
    <>
      <NextSeo title={"About"} />

      <Container>
        <Stack
          as={Box}
          maxW={"3xl"}
          mx={"1.5"}
          rounded={"md"}
          marginTop={"4"}
          marginBottom={"40"}
          textAlign={"center"}
          spacing={{ base: 8, md: 14 }}
          py={{ base: 20, md: 36 }}
        >
          {/* This is the hello starter*/}
          <Text color={"gray.100"}>
            I am a full-stack developer based in Canada, Ontario who enjoys
            coding. I started coding since I was 8 years old so I have many
            experiences with coding languages and customer sastifaction. I grew
            up with 5 other siblings at some points could be hectic but it
            taught me to be responsibile and wise with time.
          </Text>
        </Stack>
      </Container>
    </>
  );
};

export default About;
