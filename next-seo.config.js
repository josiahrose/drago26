export default {
  openGraph: {
    type: "website",
    locale: "en_IE",
    url: "https://www.josiahrose.xyz",
    site_name: "Josiah Rose",
  },
  twitter: {
    handle: "@lebradev",
    site: "@site",
    cardType: "summary_large_image",
  },
};
